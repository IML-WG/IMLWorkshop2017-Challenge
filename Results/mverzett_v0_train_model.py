#Load plotting
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score

#other tools
import numpy as np
import root_numpy
import pandas
import math
from pdb import set_trace
from glob import glob
import gc
from sklearn.preprocessing import StandardScaler
datafiles = {
    'gmod' : glob('/eos/project/i/iml/IMLChallengeQG/gluons_modified/*.root'),
    'gstd' : glob('/eos/project/i/iml/IMLChallengeQG/gluons_standard/*.root'),
    'qmod' : glob('/eos/project/i/iml/IMLChallengeQG/quarks_modified/*.root'),
    'qstd' : glob('/eos/project/i/iml/IMLChallengeQG/quarks_standard/*.root'),
}

print 'Sample  --- # of files'
for i, n in datafiles.iteritems():
    print i, '---',len(n)

@np.vectorize
def rings(jeta, jphi, comps_pt, comps_eta, comps_phi):
    drs = np.sqrt((jeta-comps_eta)**2+(jphi-comps_phi)**2)
    nbins = 10
    binid = np.floor(drs*nbins/0.4) #10 bins in DR
    retvals = [0. for i in range(nbins)]
    for pt, ibin in zip(comps_pt, binid):
        ibin = int(ibin)
        if ibin >= nbins: ibin = nbins-1
        retvals[ibin] += pt
    return tuple(retvals)

@np.vectorize
def sigmas(jpt, jeta, jphi, comps_pt, comps_eta, comps_phi):
    if not len(comps_pt):
        return 0., 0., 0.
    m11 = (comps_pt**2*(comps_eta-jeta)**2).sum()
    m22 = (comps_pt**2*(comps_phi-jphi)**2).sum()
    m12 = -1*(comps_pt**2*(comps_phi-jphi)*(comps_eta-jeta)).sum()
    ###
    t = m11+m22
    d = m11*m22 - pow(m12,2)
    #eigenvalues
    pow_val = pow(t,2)/4 - d
    if pow_val < 0:
        return 0., 0., 0.
    l1 = t/2 + math.sqrt(pow_val)    
    l2 = t/2 - math.sqrt(pow_val)
    sumpt = (comps_pt**2).sum()
    #sigmas
    sigma1 = math.sqrt(l1/sumpt)
    if l2 < 0:
        l2 = 0.
    sigma2 = math.sqrt(l2/sumpt)
    sigma = math.sqrt(pow(sigma1, 2)+pow(sigma2, 2))
    return sigma1, sigma2, sigma

@np.vectorize
def ptD(comps_pt):
    if not len(comps_pt):
        return 0.
    return math.sqrt((comps_pt**2).sum())/comps_pt.sum()

@np.vectorize
def jetEem(towerEem):
    return towerEem.sum()

@np.vectorize
def jetEhad(towerEem):
    return towerEem.sum()

scaler = None
def load(infiles, features, **kwargs):
    arr = pandas.DataFrame(
        root_numpy.root2array(
            infiles, 'treeJets',
            **kwargs
            )
        )
    s1, s2, s3 = sigmas(
        arr['jetPt'], arr['jetEta'], arr['jetPhi'],
        arr['trackPt'], arr['trackEta'], arr['trackPhi']
    )
    arr['trk_sigma1'] = s1
    arr['trk_sigma2'] = s2
    arr['trk_sigma' ] = s3
    arr['trk_ptD'] = ptD(arr['trackPt'])
    calo_s1, calo_s2, calo_s3 = sigmas(
        arr['jetPt'], arr['jetEta'], arr['jetPhi'], 
        arr['towerE'], arr['towerEta'], arr['towerPhi']
    )
    calo_ptDval = ptD(arr['towerE'])
    arr['calo_sigma1'] = calo_s1
    arr['calo_sigma2'] = calo_s2
    arr['calo_sigma' ] = calo_s3
    arr['calo_ptD'] = ptD(arr['towerE'])
    
    arr['jetEem']  = jetEem(arr['towerEem'])
    arr['jetEhad'] = jetEhad(arr['towerEhad'])

    trk_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['trackPt'], arr['trackEta'], arr['trackPhi']
        )
    for idx, ring in enumerate(trk_rings):
        name = 'trk_ring%d' % idx
        arr[name] = ring

    hcal_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['towerEhad'], arr['towerEta'], arr['towerPhi']
        )
    for idx, ring in enumerate(hcal_rings):
        name = 'hcal_ring%d' % idx
        arr[name] = ring

    hem_rings = rings(
        arr['jetEta'], arr['jetPhi'], 
        arr['towerEem'], arr['towerEta'], arr['towerPhi']
        )
    for idx, ring in enumerate(hem_rings):
        name = 'hem_ring%d' % idx
        arr[name] = ring

    arr = arr[features]
    if scaler:
        print 'scaling'
        cols = arr.columns
        arr = pandas.DataFrame(scaler.transform(arr))
        arr.columns = cols
    return arr

#
# Define features used for classification
#
feats = [
    'trk_sigma1', 'trk_sigma2', 'trk_sigma', 'trk_ptD',
    'calo_sigma1', 'calo_sigma2', 'calo_sigma', 'calo_ptD',
    'jetPt', 'jetEta', 'jetMass', 'ntracks', 'ntowers', 
    'jetEem', 'jetEhad'
]
for idx in range(10):
    feats.append('trk_ring%d' % idx)
    feats.append('hcal_ring%d' % idx)
    feats.append('hem_ring%d' % idx)


#
# Compute reweighting to make std MC look like mod
#
from sklearn.model_selection import train_test_split
from hep_ml.reweight import GBReweighter
from sklearn.ensemble import GradientBoostingClassifier
def check_discrimination(stds, mods, weights, label):
    weights = np.array(weights)
    weights /= 1. * np.mean(weights)

    X = np.vstack([stds, mods])
    y = np.concatenate((np.zeros(stds.shape[0]), np.ones(mods.shape[0])))
    sample_weights = np.concatenate((weights, np.ones(mods.shape[0])))
    
    trainX, testX, trainY, testY, trainW, testW = train_test_split(X, y, sample_weights, train_size=0.45, random_state=42)
    
    # using only 30 trees for speed 
    clf = GradientBoostingClassifier(n_estimators=50, max_depth=4,random_state=42, verbose=1)
    clf.fit(trainX, trainY, sample_weight=trainW)
    test_proba = clf.predict_proba(testX)
    
    plt.figure(figsize=[8, 8])
    plt.plot(*roc_curve(testY,  test_proba[:, 1], sample_weight=testW)[:2], label='Mod-Std separation test')
    print label, 'ROC AUC', roc_auc_score(testY,  test_proba[:, 1], sample_weight=testW)
    plt.plot([0, 1], [0, 1], 'k--', label='no discrimination')
    plt.xlabel('FPR'), plt.ylabel('FPR')
    plt.legend(loc='best')
    plt.plot()
    plt.savefig(label)
    plt.clf()

reweighter = GBReweighter(n_estimators=50, max_depth=3, learning_rate=0.1)
nfiles_for_bdt = 300
modset = load(datafiles['gmod'][:nfiles_for_bdt] + datafiles['qmod'][:nfiles_for_bdt], feats)
scaler = StandardScaler()
columns = modset.columns
modset = pandas.DataFrame(scaler.fit_transform(modset))
modset.columns = columns
stdset = load(datafiles['gstd'][:nfiles_for_bdt] + datafiles['qstd'][:nfiles_for_bdt], feats)
print '\n\nloaded:'
print '  std MC:', modset.shape[0]
print '  mod MC:', stdset.shape[0]
print '\n\n'

train_mod, test_mod = train_test_split(modset, train_size=0.4, random_state=42)
train_std, test_std = train_test_split(stdset, train_size=0.4, random_state=42)

check_discrimination(test_std, test_mod, np.ones(test_std.shape[0]), 'initial_separation.png')
reweighter.fit(train_std, train_mod) #make std look like mod
weights = reweighter.predict_weights(test_std) #1/w to be used
check_discrimination(test_std, test_mod, weights, 'BDTReweighted_separation.png')

del modset
del stdset
gc.collect()

#
# Make Keras model
#
print 'creating keras model'
nepochs= 20
nsub_epochs= 80 
batchsize=15000
startlearnrate=0.0003
lrdecrease=0.000025
lreeveryep=1
lrthresh=0.000025
useweights=False
splittrainandtest=0.85
maxqsize=10 #sufficient

from keras.layers import Dense, Dropout, Flatten, Convolution2D, merge, Convolution1D, Input
from keras.models import Model

inputs = Input(shape=(len(feats),))
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform', input_shape=(len(feats),))(inputs)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
x = Dropout(0.1)(x)
x = Dense(len(feats)*2, activation='relu', kernel_initializer='lecun_uniform')(x)
predictions = Dense(2, activation='softmax',kernel_initializer='lecun_uniform')(x)
model = Model(inputs=inputs, outputs=predictions)

print('compiling')

from keras.optimizers import Adam
adam = Adam(lr=startlearnrate)
model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])

# This stores the history of the training to e.g. allow to plot the learning curve

from keras.callbacks import History, LearningRateScheduler, EarlyStopping #, ReduceLROnPlateau # , TensorBoard
# loss per epoch
history = History()

#stop when val loss does not decrease anymore
stopping = EarlyStopping(monitor='val_loss', patience=5, verbose=1, mode='min')

from ReduceLROnPlateau import ReduceLROnPlateau

LR_onplatCB = ReduceLROnPlateau(
    monitor='val_loss', factor=0.5, patience=2, 
    mode='auto', verbose=1, epsilon=0.001, cooldown=0, min_lr=0.00001
)


##from learningRateCallback import learningRateDecrease
##lrdecr_cb = learningRateDecrease(lreeveryep, lrdecrease, startlearnrate,1,lrthresh)
##
##LearningRateScheduler(lrdecr_cb.reducelearnrate)

from keras.utils import np_utils
def load_dataset(qfiles, gfiles):
    print 'loading files'
    quarks = load(qfiles, feats)    
    gluons = load(gfiles, feats)

    isQ = np.concatenate((np.ones(quarks.shape[0]), np.zeros(gluons.shape[0])))
    Y = np_utils.to_categorical(isQ.astype(int), 2) #isG, isQ
    X = pandas.concat([quarks, gluons])

    print 'computing weights'
    weights = reweighter.predict_weights(X)
    weights = np.array(weights)
    weights /= 1. * np.mean(weights)
    return X, Y, weights

ntrain = 2000
nval = 2000 
train_files = [datafiles['qstd'][:ntrain], datafiles['gstd'][:ntrain]]
val_files   = [datafiles['qstd'][nval:], datafiles['gstd'][nval:]]

val_x, val_y, val_weights = load_dataset(val_files[0], val_files[1])

from itertools import cycle
def deliver_data(nbatch):
    gluons = []
    quarks = []
    for iq, ig in cycle(zip(*train_files)):
        quarks.append(iq)
        gluons.append(ig)
        if len(quarks) == nbatch:
            yield quarks, gluons
            quarks, gluons = [], []

epoch = 0
nfiles_per_batch = 800
for qqs, ggs in deliver_data(nfiles_per_batch):
    if epoch == nepochs:        
        break
    print 'Training epoch', epoch
    epoch += 1
    xtrain, labels, weights = load_dataset(qqs, ggs)
    gc.collect()

    model.fit(
        xtrain.as_matrix(), labels, batch_size=batchsize, epochs=nsub_epochs,
        callbacks=[history,stopping,LR_onplatCB], sample_weight=weights,
        validation_data=(val_x.as_matrix(), val_y, val_weights), verbose=1
        )


print 'trainig done!'
model.save("KERAS_model.h5")

# summarize history for loss for trainin and test sample
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('learningcurve.png') 
#plt.show()

plt.figure(2)
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('acc')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('accuracycurve.png')

val_pred = model.predict(val_x.as_matrix())[:, 1]
plt.figure(figsize=[8, 8])
plt.plot(
    *roc_curve(val_y[:, 1],  val_pred, sample_weight=val_weights)[:2], 
    label='ROC on validation sample', color='b')
print 'ROC AUC on validation:', roc_auc_score(val_y[:, 1],  val_pred, sample_weight=val_weights)
plt.plot([0, 1], [0, 1], 'k--', label='no discrimination')

ntest = -200
test_x, test_y, test_weights = load_dataset(datafiles['qmod'][ntest:], datafiles['gmod'][ntest:])
test_pred = model.predict(test_x.as_matrix())[:, 1]
plt.plot(
    *roc_curve(test_y[:, 1],  test_pred)[:2], 
    label='ROC on test sample', color='g'
)
print 'ROC AUC on TEST:', roc_auc_score(test_y[:, 1],  test_pred)

plt.xlabel('FPR'), plt.ylabel('FPR')
plt.legend(loc='best')
plt.plot()
plt.savefig('FinalROC.png')
plt.clf()


