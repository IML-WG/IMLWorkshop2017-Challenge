#!/usr/bin/env python
# Author: Stefan Wunsch, 2017


#'''
#Example preprocessing of gluons and quark jets data
#from IML challenge
#'''


def process_arguments():
	import argparse
	#'''
	#Process input argument
	#Returns:
	#  args: command-line arguments
	#'''
	parser = argparse.ArgumentParser(description='Preprocess data for IML'+\
			'challenge. We read in the data and apply zero-padding on the'+\
			'dynamic value of tracks and tower. In detail, this means, e.g.,'+\
			'that if we fix the number of tracks to 5 but there are only 3'+\
			'tracks, the other tracks variables are filled with zeros.')
	parser.add_argument('--gluons', '-g', type=str, default='',
			help='ROOT files with gluon jets')
	parser.add_argument('--quarks', '-q', type=str, default='',
			help='ROOT files with quark jets')
	parser.add_argument('--tracks', '-tr', type=int, default=5,
			help='Number of towers added to the output file')
	parser.add_argument('--towers', '-to', type=int, default=5,
			help='Number of towers added to the output file')
	parser.add_argument('--output', '-o', type=str, default='preprocessed_data.root',
			help='Output ROOT file with combined data from input files')

	args = parser.parse_args()
	if args.gluons == '':
		raise Exception('Please specify ROOT files for option --gluons')
	if args.quarks == '':
		raise Exception('Please specify ROOT files for option --quarks')
	
	return args


def combine_files(filenames, tree):
	#'''
	# Take filenames of ROOT files and combine them in a TChain
	#
	# Args:
	#	  filenames: String with filenames separated by blanks
	#	  tree: Name of tree in files
	#
	#	 Returns:
	
	#chain: TChain that holds data of all input ROOT files
	# '''
	
	chain = ROOT.TChain(tree)
	for filename in filenames.strip().split(' '):
		chain.AddFile(filename)
	
	return chain


def add_tree(output_file, tree_name, chain, num_towers, num_tracks):
	#'''
	# Add tree to output ROOT file with zero-padded events from chain
	#
	#Args:
	#	  output_file: Output ROOT file
	#	  tree_name: Name of created tree
	#	  chain: TChain with events
	#	  num_towers: Number of tower that should be kept or zero-padded
	#	  num_tracks: Number of tracks that should be kept or zero-padded
	#'''
	
	from array import array
	
	tree = ROOT.TTree(tree_name, tree_name)
	
	# Define variables
	variables = {}
	single_variables = ['jetPt', 'jetEta', 'jetPhi', 'jetMass', 'ntracks', 'ntowers']
	for name in single_variables:
		variables[name] = array('f', [-999])
		tree.Branch(name, variables[name], '{0}/F'.format(name))
	
	track_variables = ['trackPt', 'trackEta', 'trackPhi', 'trackCharge']
	for name in track_variables:
	   for i in range(num_tracks):
			name_ = '{0}_{1}'.format(name, i)
			variables[name_] = array('f', [-999])
	  		tree.Branch(name_, variables[name_], '{0}/F'.format(name_))
	
	tower_variables = ['towerE', 'towerEem', 'towerEhad', 'towerEta', 'towerPhi']
	for name in tower_variables:
	   for i in range(num_towers):
			name_ = '{0}_{1}'.format(name, i)
	  		variables[name_] = array('f', [-999])
	  		tree.Branch(name_, variables[name_], '{0}/F'.format(name_))
			
	DRweighted=array('f', [-999])
	fRing0=array('f', [-999])
	fRing1=array('f', [-999])
	fRing2=array('f', [-999])
	fRing3=array('f', [-999])
	ptD=array('f', [-999])
	jetR=array('f', [-999])
	ave_deta=array('f', [-999])
	ave_dphi=array('f', [-999])
	axis1 = array('f', [-999])
	axis2 = array('f', [-999])
	tree.Branch('DRweighted', DRweighted, 'DRweighted/F')
	tree.Branch('fRing0', fRing0, 'fRing0/F')
	tree.Branch('fRing1', fRing1, 'fRing1/F')
	tree.Branch('fRing2', fRing2, 'fRing2/F')
	tree.Branch('fRing3', fRing3, 'fRing3/F')
	tree.Branch('ptD', ptD, 'ptD/F')
	tree.Branch('jetR', jetR, 'jetR/F')
	tree.Branch('ave_deta', ave_deta, 'ave_deta/F')
	tree.Branch('ave_dphi', ave_dphi, 'ave_dphi/F')
	tree.Branch('axis1', axis1, 'axis1/F')
	tree.Branch('axis2', axis2, 'axis2/F')

	vJet = ROOT.TLorentzVector(0,0,0,0)
	vTrack = ROOT.TLorentzVector(0,0,0,0)
	sum_rings=array('f', [-999,-999,-999,-999,-999])
	nRings=5

	# Load events from chain and push to output file
	for event in chain:
		# Add single variables
		for name in single_variables:
			variables[name][0] = getattr(event, name)

		vJet.SetPtEtaPhiM(variables['jetPt'][0], variables['jetEta'][0], variables['jetPhi'][0], variables['jetMass'][0])
		#vJet.Print()

		# Add track variables
		for name in track_variables:
			values = getattr(event, name)
			# Set all variables to zero
			for i in range(num_tracks):
				variables['{0}_{1}'.format(name, i)][0] = 0.0
			# Set variables and potentially leave the zeros untouched
			for i in range(min(num_tracks, len(values))):
				variables['{0}_{1}'.format(name, i)][0] = values[i]

		sumWdR2 = 0.
		sumW = 0.
		sumW2 = 0.
		sum_deta = 0.
		sum_dphi = 0.
		sum_deta2 = 0.
		sum_detadphi = 0.
		sum_dphi2 = 0.
		pTMax=-1.
		for i in range(nRings):
			sum_rings[i]=0.

		values_Pt = getattr(event, 'trackPt')
		values_Eta = getattr(event, 'trackEta')
		values_Phi = getattr(event, 'trackPhi')
		for i in range(len(values_Pt)):
			vTrack.SetPtEtaPhiM(values_Pt[i], values_Eta[i], values_Phi[i], 0)
			dR = ROOT.TMath.Sqrt((vTrack.Eta()-vJet.Eta())*(vTrack.Eta()-vJet.Eta()) + (vTrack.Phi()-vJet.Phi())* (vTrack.Phi()-vJet.Phi()))
			weight = vTrack.Pt()
			weight2 = weight*weight
			sumWdR2 = sumWdR2 + weight2 * dR * dR
			sumW = sumW + weight
			sumW2 = sumW2 + weight2

			deta = vTrack.Eta()-vJet.Eta()
			dphi = vTrack.Phi()-vJet.Phi()
			sum_deta = sum_deta + deta*weight2
			sum_dphi = sum_dphi + dphi*weight2
			sum_deta2 = sum_deta2 + deta*deta*weight2
			sum_detadphi = sum_detadphi + ROOT.TMath.Abs(deta)*dphi*weight2
			sum_dphi2 = sum_dphi2 + dphi*dphi*weight2

			if(vTrack.Pt()>pTMax):
				ptMax = vTrack.Pt()

			index = int(ROOT.TMath.Floor(dR*10))
			if (index>nRings-1):
				index=int(nRings-1)
			sum_rings[index] = sum_rings[index] + weight

		if (sumW>0):
			DRweighted[0]=sumWdR2/sumW2
			fRing0[0]=sum_rings[0]/vJet.Pt()
			fRing1[0]=sum_rings[1]/vJet.Pt()
			fRing2[0]=sum_rings[2]/vJet.Pt()
			fRing3[0]=sum_rings[3]/vJet.Pt()
			ptD[0]=ROOT.TMath.Sqrt(sumW2/sumW)
			jetR[0]=ptMax/sumW
			ave_deta[0]=sum_deta/sumW2
			ave_dphi[0]=sum_dphi/sumW2

			covMatrix=ROOT.TMatrixDSym(2)
			covMatrix[0][0] = sum_deta2/sumW2
			covMatrix[0][1] = sum_detadphi/sumW2
			covMatrix[1][1] = sum_dphi2/sumW2
			covMatrix[1][0] = covMatrix[0][1]
			eigVals=ROOT.TMatrixDSymEigen(covMatrix).GetEigenValues()
			axis1[0] = ROOT.TMath.Sqrt(ROOT.TMath.Abs(eigVals(0)));
			axis2[0] = ROOT.TMath.Sqrt(ROOT.TMath.Abs(eigVals(1)));
			if( axis1[0] < axis2[0] ):
				tmp=axis1[0]
				axis1[0]=axis2[0]
				axis2[0]=tmp
		else:
			DRweighted[0]=-1.
			fRing0[0]=-1.
			fRing1[0]=-1.
			fRing2[0]=-1.
			fRing3[0]=-1.
			ptD[0]=-1.
			jetR[0]=-1.
			ave_deta[0]=-1.
			ave_dphi[0]=-1.
			axis1[0] = -1.
			axis2[0] = -1.

		# Add tower variables
		for name in tower_variables:
			values = getattr(event, name)
			# Set all variables to zero
			for i in range(num_towers):
				variables['{0}_{1}'.format(name, i)][0] = 0.0
			# Set variables and potentially leave the zeros untouched
			for i in range(min(num_towers, len(values))):
				variables['{0}_{1}'.format(name, i)][0] = values[i]

		tree.Fill()

	# Write tree to file
	tree.Write()


if __name__=='__main__':
	# Get arguments
	args = process_arguments()

	# Combine input ROOT files
	import ROOT # import this here so that it does not overwrite our argparse config
	chain_gluons = combine_files(args.gluons, 'treeJets')
	chain_quarks = combine_files(args.quarks, 'treeJets')

	print('Loaded {0} quark jet events from {1} files.'.format(
		chain_quarks.GetEntries(), len(args.quarks.strip().split(' '))))
	print('Loaded {0} gluon jet events from {1} files.'.format(
		chain_gluons.GetEntries(), len(args.gluons.strip().split(' '))))

	# Create output ROOT file
	output = ROOT.TFile(args.output, 'RECREATE')

	# Do zero-padding and add trees to output ROOT file
	add_tree(output, 'gluons', chain_gluons, args.towers, args.tracks)
	add_tree(output, 'quarks', chain_quarks, args.towers, args.tracks)

	# Write file and close
	output.Write()
	output.Close()
